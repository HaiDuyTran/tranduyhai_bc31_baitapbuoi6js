/**
Input:


Các bước xử lý:
-Cho số nguyên nhỏ nhất = 0 và sum = 0
-Dùng for  để cộng sum +=số nguyên nhỏ nhất
-Nếu sum >=100000 thì break để kết thúc vòng lặp

Output: 
Log ra số nguyên dương nhỏ nhất

 */



function timSoNguyenNN() {
  var sum = 0;
  for (var soNguyenNNValue = 0; soNguyenNNValue < 10000; soNguyenNNValue++) {
    sum += soNguyenNNValue;
    if (sum >= 10000) {
      break;
    }
  }
  console.log(soNguyenNNValue);
  document.getElementById("timSo").innerText = `Số nguyên dương nhỏ nhất là: `+soNguyenNNValue;
}
