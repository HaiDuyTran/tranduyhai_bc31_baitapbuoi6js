/**
Input:
Nhập biến X 
Nhập biến N 

Các bước xử lý:
-Dom 2 giá trị x và n
-Cho tổng ban đầu bằng X
-Dùng Math.pow để tính lũy thừa
-cho for lặp từ i=2 đến i=n 
-Lũy thừa value = lũy thừa của x mũ i 
-Tổng += lũy thừa value

Output
Tổng cần tính
 */

function tinhTong() {
  var soX_Value = document.getElementById("txt-so-x").value * 1;
  var soN_Value = document.getElementById("txt-so-n").value * 1;
  var sum = soX_Value;

  for (var i = 2; i <= soN_Value; i++) {
    var luyThuaValue = Math.pow(soX_Value, i);
    sum += luyThuaValue;
  }
  document.getElementById("txt-tinh-tong").innerText = sum;
}
