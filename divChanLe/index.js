/**
Input:

Các bước xử lý:
Cho vòng for lặp 10 lần
-dùng document.createElement("div") và document.body.appendChild(divs)
để tạo thẻ div
-với i chia hết cho 2 =>i chẵn : cho bg red và thêm chữ "Div chẵn"
-Ngc lại i lẻ: cho bg blue và thêm chữ "Div lẻ"

Output:
Tạo ra 10 thẻ div
 */

function taoDiv() {
  for (var i = 0; i < 10; i++) {
    var divs = document.createElement("div");
    document.body.appendChild(divs);
    if (i % 2 == 0) {
      divs.style.background = "red";
      divs.innerHTML = "<span style='color:white'>Div chẵn</span>";
    } else {
      divs.style.background = "blue";
      divs.innerHTML = "<span style='color:white'>Div lẻ</span>";
    }
  }
}
