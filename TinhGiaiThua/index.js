/**
Input:Nhập số n

Các bước xử lý;
Cho giaiThuaValue ban đầu = 1
-Vòng lặp for i từ 1 đến n
 giaiThuaValue = giaiThuaValue * i

 Output:
 Giai thừa của n
 */

function tinhGiaiThua() {
  var soN_Value = document.getElementById("txt-so-n").value * 1;
  var giaiThuaValue = 1;
  for (var i = 1; i <= soN_Value; i++) {
    giaiThuaValue = giaiThuaValue * i;
  }
  document.getElementById("tinh-giai-thua").innerText = giaiThuaValue;
}
